<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>套餐管理管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }

	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/client/clientPlan/">套餐管理列表</a></li>
		<shiro:hasPermission name="client:clientPlan:edit"><li><a href="${ctx}/client/clientPlan/form">套餐管理添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="clientPlan" action="${ctx}/client/clientPlan/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>套餐名称：</label>
				<form:input path="name" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>套餐名称</th>
				<th>套餐价格</th>
				<th>更新者</th>
				<th>更新时间</th>
				<shiro:hasPermission name="client:clientPlan:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="clientPlan">
			<tr>
				<td><a href="${ctx}/client/clientPlan/form?id=${clientPlan.id}">
					${clientPlan.name}
				</a></td>
				<td>
					${clientPlan.price}
				</td>
				<td>
					${clientPlan.updateBy.id}
				</td>
				<td>
					<fmt:formatDate value="${clientPlan.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<shiro:hasPermission name="client:clientPlan:edit"><td>
    				<a href="${ctx}/client/clientPlan/form?id=${clientPlan.id}">修改</a>
					<a href="${ctx}/client/clientPlan/delete?id=${clientPlan.id}" onclick="return confirmx('确认要删除该套餐管理吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>