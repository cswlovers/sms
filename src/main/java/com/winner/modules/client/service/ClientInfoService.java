/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.winner.modules.client.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.winner.common.persistence.Page;
import com.winner.common.service.CrudService;
import com.winner.modules.client.entity.ClientInfo;
import com.winner.modules.client.dao.ClientInfoDao;

/**
 * 客户管理Service
 * @author chengshaowei
 * @version 2016-06-20
 */
@Service
@Transactional(readOnly = true)
public class ClientInfoService extends CrudService<ClientInfoDao, ClientInfo> {

	public ClientInfo get(String id) {
		return super.get(id);
	}
	
	public List<ClientInfo> findList(ClientInfo clientInfo) {
		return super.findList(clientInfo);
	}
	
	public Page<ClientInfo> findPage(Page<ClientInfo> page, ClientInfo clientInfo) {
		return super.findPage(page, clientInfo);
	}
	
	@Transactional(readOnly = false)
	public void save(ClientInfo clientInfo) {
		super.save(clientInfo);
	}
	
	@Transactional(readOnly = false)
	public void delete(ClientInfo clientInfo) {
		super.delete(clientInfo);
	}
	
}