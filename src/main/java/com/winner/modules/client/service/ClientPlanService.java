/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.winner.modules.client.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.winner.common.persistence.Page;
import com.winner.common.service.CrudService;
import com.winner.modules.client.entity.ClientPlan;
import com.winner.modules.client.dao.ClientPlanDao;

/**
 * 套餐管理Service
 * @author chengshaowei
 * @version 2016-06-20
 */
@Service
@Transactional(readOnly = true)
public class ClientPlanService extends CrudService<ClientPlanDao, ClientPlan> {

	public ClientPlan get(String id) {
		return super.get(id);
	}
	
	public List<ClientPlan> findList(ClientPlan clientPlan) {
		return super.findList(clientPlan);
	}
	
	public Page<ClientPlan> findPage(Page<ClientPlan> page, ClientPlan clientPlan) {
		return super.findPage(page, clientPlan);
	}
	
	@Transactional(readOnly = false)
	public void save(ClientPlan clientPlan) {
		super.save(clientPlan);
	}
	
	@Transactional(readOnly = false)
	public void delete(ClientPlan clientPlan) {
		super.delete(clientPlan);
	}
	
}