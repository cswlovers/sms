/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.winner.modules.client.entity;

import org.hibernate.validator.constraints.Length;

import com.winner.common.persistence.DataEntity;

import java.math.BigDecimal;

/**
 * 套餐管理Entity
 * @author chengshaowei
 * @version 2016-06-20
 */
public class ClientPlan extends DataEntity<ClientPlan> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 套餐名称
	private BigDecimal price;		// 套餐价格
	private String content;		// 套餐介绍
	private BigDecimal beginPrice;		// 开始 套餐价格
	private BigDecimal endPrice;		// 结束 套餐价格
	
	public ClientPlan() {
		super();
	}

	public ClientPlan(String id){
		super(id);
	}

	@Length(min=1, max=64, message="套餐名称长度必须介于 1 和 64 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getBeginPrice() {
		return beginPrice;
	}

	public void setBeginPrice(BigDecimal beginPrice) {
		this.beginPrice = beginPrice;
	}

	public BigDecimal getEndPrice() {
		return endPrice;
	}

	public void setEndPrice(BigDecimal endPrice) {
		this.endPrice = endPrice;
	}
}