/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.winner.modules.client.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.NotNull;

import com.winner.common.persistence.DataEntity;

/**
 * 客户管理Entity
 * @author chengshaowei
 * @version 2016-06-20
 */
public class ClientInfo extends DataEntity<ClientInfo> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 客户姓名
	private String phone;		// 客户联系电话
	private String email;		// 客户邮箱
	private Date subscribeDate;		// 预约时间
	private Date shootingDate;		// 拍摄时间
	private Date delivery;		// 领取时间
	private String state;		// 状态
	private String photos;		// 客户照片
	private Date beginSubscribeDate;		// 开始 预约时间
	private Date endSubscribeDate;		// 结束 预约时间
	
	public ClientInfo() {
		super();
	}

	public ClientInfo(String id){
		super(id);
	}

	@Length(min=1, max=64, message="客户姓名长度必须介于 1 和 64 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=1, max=64, message="客户联系电话长度必须介于 1 和 64 之间")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Length(min=0, max=64, message="客户邮箱长度必须介于 0 和 64 之间")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="预约时间不能为空")
	public Date getSubscribeDate() {
		return subscribeDate;
	}

	public void setSubscribeDate(Date subscribeDate) {
		this.subscribeDate = subscribeDate;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getShootingDate() {
		return shootingDate;
	}

	public void setShootingDate(Date shootingDate) {
		this.shootingDate = shootingDate;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getDelivery() {
		return delivery;
	}

	public void setDelivery(Date delivery) {
		this.delivery = delivery;
	}
	
	@Length(min=1, max=2, message="状态长度必须介于 1 和 2 之间")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@Length(min=0, max=2000, message="客户照片长度必须介于 0 和 2000 之间")
	public String getPhotos() {
		return photos;
	}

	public void setPhotos(String photos) {
		this.photos = photos;
	}
	
	public Date getBeginSubscribeDate() {
		return beginSubscribeDate;
	}

	public void setBeginSubscribeDate(Date beginSubscribeDate) {
		this.beginSubscribeDate = beginSubscribeDate;
	}
	
	public Date getEndSubscribeDate() {
		return endSubscribeDate;
	}

	public void setEndSubscribeDate(Date endSubscribeDate) {
		this.endSubscribeDate = endSubscribeDate;
	}
		
}