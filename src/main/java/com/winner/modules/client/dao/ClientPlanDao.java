/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.winner.modules.client.dao;

import com.winner.common.persistence.CrudDao;
import com.winner.common.persistence.annotation.MyBatisDao;
import com.winner.modules.client.entity.ClientPlan;

/**
 * 套餐管理DAO接口
 * @author chengshaowei
 * @version 2016-06-20
 */
@MyBatisDao
public interface ClientPlanDao extends CrudDao<ClientPlan> {
	
}