/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.winner.modules.client.dao;

import com.winner.common.persistence.CrudDao;
import com.winner.common.persistence.annotation.MyBatisDao;
import com.winner.modules.client.entity.ClientInfo;

/**
 * 客户管理DAO接口
 * @author chengshaowei
 * @version 2016-06-20
 */
@MyBatisDao
public interface ClientInfoDao extends CrudDao<ClientInfo> {
	
}