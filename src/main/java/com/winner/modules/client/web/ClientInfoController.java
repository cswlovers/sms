/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.winner.modules.client.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.winner.common.config.Global;
import com.winner.common.persistence.Page;
import com.winner.common.web.BaseController;
import com.winner.common.utils.StringUtils;
import com.winner.modules.client.entity.ClientInfo;
import com.winner.modules.client.service.ClientInfoService;

/**
 * 客户管理Controller
 * @author chengshaowei
 * @version 2016-06-20
 */
@Controller
@RequestMapping(value = "${adminPath}/client/clientInfo")
public class ClientInfoController extends BaseController {

	@Autowired
	private ClientInfoService clientInfoService;
	
	@ModelAttribute
	public ClientInfo get(@RequestParam(required=false) String id) {
		ClientInfo entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = clientInfoService.get(id);
		}
		if (entity == null){
			entity = new ClientInfo();
		}
		return entity;
	}
	
	@RequiresPermissions("client:clientInfo:view")
	@RequestMapping(value = {"list", ""})
	public String list(ClientInfo clientInfo, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ClientInfo> page = clientInfoService.findPage(new Page<ClientInfo>(request, response), clientInfo); 
		model.addAttribute("page", page);
		return "modules/client/clientInfoList";
	}

	@RequiresPermissions("client:clientInfo:view")
	@RequestMapping(value = "form")
	public String form(ClientInfo clientInfo, Model model) {
		model.addAttribute("clientInfo", clientInfo);
		return "modules/client/clientInfoForm";
	}

	@RequiresPermissions("client:clientInfo:edit")
	@RequestMapping(value = "save")
	public String save(ClientInfo clientInfo, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, clientInfo)){
			return form(clientInfo, model);
		}
		clientInfoService.save(clientInfo);
		addMessage(redirectAttributes, "保存客户管理成功");
		return "redirect:"+Global.getAdminPath()+"/client/clientInfo/?repage";
	}
	
	@RequiresPermissions("client:clientInfo:edit")
	@RequestMapping(value = "delete")
	public String delete(ClientInfo clientInfo, RedirectAttributes redirectAttributes) {
		clientInfoService.delete(clientInfo);
		addMessage(redirectAttributes, "删除客户管理成功");
		return "redirect:"+Global.getAdminPath()+"/client/clientInfo/?repage";
	}

}