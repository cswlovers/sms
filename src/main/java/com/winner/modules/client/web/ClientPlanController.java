/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.winner.modules.client.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.winner.common.config.Global;
import com.winner.common.persistence.Page;
import com.winner.common.web.BaseController;
import com.winner.common.utils.StringUtils;
import com.winner.modules.client.entity.ClientPlan;
import com.winner.modules.client.service.ClientPlanService;

/**
 * 套餐管理Controller
 * @author chengshaowei
 * @version 2016-06-20
 */
@Controller
@RequestMapping(value = "${adminPath}/client/clientPlan")
public class ClientPlanController extends BaseController {

	@Autowired
	private ClientPlanService clientPlanService;
	
	@ModelAttribute
	public ClientPlan get(@RequestParam(required=false) String id) {
		ClientPlan entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = clientPlanService.get(id);
		}
		if (entity == null){
			entity = new ClientPlan();
		}
		return entity;
	}
	
	@RequiresPermissions("client:clientPlan:view")
	@RequestMapping(value = {"list", ""})
	public String list(ClientPlan clientPlan, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ClientPlan> page = clientPlanService.findPage(new Page<ClientPlan>(request, response), clientPlan); 
		model.addAttribute("page", page);
		return "modules/client/clientPlanList";
	}

	@RequiresPermissions("client:clientPlan:view")
	@RequestMapping(value = "form")
	public String form(ClientPlan clientPlan, Model model) {
		model.addAttribute("clientPlan", clientPlan);
		return "modules/client/clientPlanForm";
	}

	@RequiresPermissions("client:clientPlan:edit")
	@RequestMapping(value = "save")
	public String save(ClientPlan clientPlan, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, clientPlan)){
			return form(clientPlan, model);
		}
		clientPlanService.save(clientPlan);
		addMessage(redirectAttributes, "保存套餐管理成功");
		return "redirect:"+Global.getAdminPath()+"/client/clientPlan/?repage";
	}
	
	@RequiresPermissions("client:clientPlan:edit")
	@RequestMapping(value = "delete")
	public String delete(ClientPlan clientPlan, RedirectAttributes redirectAttributes) {
		clientPlanService.delete(clientPlan);
		addMessage(redirectAttributes, "删除套餐管理成功");
		return "redirect:"+Global.getAdminPath()+"/client/clientPlan/?repage";
	}

}